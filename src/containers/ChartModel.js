import React, { useContext } from "react";
import Chart from "../components/Chart";

import { ChartContext } from "../context/chart";

function ChartModel() {
  const { collection } = useContext(ChartContext);
  console.log(collection);
  return (
    <div className="chart-model">
      <Chart chartName="Range Chart" collection={collection} />
      <Chart chartName="Range Chart" collection={collection} />
      <Chart chartName="Range Chart" collection={collection} />
    </div>
  );
}

export default ChartModel;
