import React, { useEffect, useState } from "react";

export const ChartContext = React.createContext();

const getNumbers = async (setCollection) => {
  const response = await fetch(
    "https://www.random.org/integers/?num=10&min=1&max=6&col=1&base=10&format=plain&rnd=new"
  );
  const data = await response.text();
  const numbers = data.replace(/^\s+|\s+$/g, "").split(/\s+/);
  const collection = numbers.map((number) => [
    +number,
    Math.floor(Date.now() / 1000),
  ]);
  setCollection(collection);
};

export const ChartProvider = ({ children }) => {
  const [collection, setCollection] = useState([], () => {});

  useEffect(() => {
    getNumbers(setCollection);
  }, []);

  return (
    <ChartContext.Provider
      value={{
        collection,
      }}
    >
      {children}
    </ChartContext.Provider>
  );
};
