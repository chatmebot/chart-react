import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Chart } from "react-google-charts";

const getData = (numbers, t) =>
  numbers.reduce(
    (acc, curr) => {
      acc.push([curr[1], curr[0]]);
      return acc;
    },
    [["Date", "Range"]]
  );

const ChartView = ({ chartName, collection }) => {
  const data = getData(collection);
  console.log(data);
  return (
    <div className="chart-wrapper">
      <Chart
        width={"100%"}
        height={"500px"}
        chartType={"LineChart"}
        data={data}
        options={{
          hAxis: {
            title: "Time",
          },
          vAxis: {
            title: "Range",
          },
          title: chartName,
        }}
      />
    </div>
  );
};

ChartView.propTypes = {
  collection: PropTypes.array,
  // fetchAnalytics: PropTypes.func,
  isFetching: PropTypes.bool,
  chartName: PropTypes.string,
};

export default ChartView;
