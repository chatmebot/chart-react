import React from "react";

import  ChartModel  from "./containers/ChartModel";

function App() {
    return (
      <div className="App">
		<ChartModel/>
      </div>
    );
}

export default App;
